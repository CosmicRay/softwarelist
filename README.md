# Software List

Personal tracking list of software (info is not comprehensively accurate in regard to OS and versioning)

| Name               |       OS       | Notes                                                        |
| ------------------ | :------------: | ------------------------------------------------------------ |
| Patch My PC        |     `WIN`      | auto-update program, supports many programs                  |
| Discord            | `WIN` `LINUX`  | voice and text comm. app                                     |
| Typora             | `WIN` `LINUX`  | markdown text editor                                         |
| Teamspeak          | `WIN` `LINUX`  | voice and text comm. app                                     |
| WinCompose         |     `WIN`      | tool to macro alt codes via a compose key binding            |
| AutoHotKey         |     `WIN`      | write scripts to automate windows environment and macros     |
| Visual Studio Code | `WIN` `LINUX`  | code / text editor                                           |
| VLC                | `WIN`  `LINUX` | free video and audio player with good features               |
| ShareX             |     `WIN`      | thorough screen shot tool                                    |
| Pycharm            | `WIN` `LINUX`  | Python IDE                                                   |
| CPU-Z              |     `WIN`      | CPU info tool with benchmarking, some memory, MB, GPU info also |
| WinDirStat         |     `WIN`      | analyzes disk space with sorting and visualizes entire computer storage space as colorful blocks on a map |
| K4DirStat          |    `LINUX`     | New implementation of KDirStat, which  predates WinDirStat   |
| HWiNFO             |     `WIN`      | Live monitor many system components                          |

